package com.paycenter.client;

import com.google.gson.Gson;
import com.google.gson.JsonParser;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Properties;

import static java.security.KeyStore.getInstance;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class PayCenterClient {

    public static enum PropName {
        SECURE_CONNECTION("secure_connection"),
        HOST("host"),
        PORT("port"),
        PROJECT_ID("project_id"),
        PROJECT_AGENT_ID("project_agent_id"),
        CURRENCY("currency"),
        TRUST_STORE_FILE_NAME("trust_store_file_name"),
        KEY_STORE_FILE_NAME("key_store_file_name"),
        KEY_STORE_PASS("key_store_pass"),
        TRUST_STORE_PASS("trust_store_pass");
        public final String name;

        private PropName(String name) {
            this.name = name;
        }
    }

    private static final Gson GSON = new Gson();
    private static final String METHOD_MAKE_PAY = "/pay/make_pay";
    private static final String METHOD_GET_PAYMENT_DETAILS = "/pay/get_payment_details";
    public static final JsonParser JSON_PARSER = new JsonParser();

    private final String baseUrl;
    private final String host;
    private final String port;
    private final String projectId;
    private final String projectAgentId;
    private final String currency;
    private final KeyStore keyStore;
    private final String keyStorePass;
    private final KeyStore trustStore;
    private final boolean secureConnection;


    public PayCenterClient(Properties properties) throws NoPropertyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        this.secureConnection = getSecureProp(properties);
        this.host = getStringProp(properties, PropName.HOST);
        this.port = getStringProp(properties, PropName.PORT);
        this.projectId = getStringProp(properties, PropName.PROJECT_ID);
        this.projectAgentId = getStringProp(properties, PropName.PROJECT_AGENT_ID);
        this.currency = getStringProp(properties, PropName.CURRENCY);
        if (secureConnection) {
            String trustStoreFileName = getStringProp(properties, PropName.TRUST_STORE_FILE_NAME);
            String trustStorePass = getStringProp(properties, PropName.TRUST_STORE_PASS);
            this.trustStore = loadKeyStore(trustStoreFileName, trustStorePass);

            String keyStoreFileName = getStringProp(properties, PropName.KEY_STORE_FILE_NAME);
            keyStorePass = getStringProp(properties, PropName.KEY_STORE_PASS);
            this.keyStore = loadKeyStore(keyStoreFileName, keyStorePass);
            baseUrl = "https://" + host + ":" + port;
        } else {
            trustStore = null;
            keyStore = null;
            keyStorePass = null;
            baseUrl = "http://" + host + ":" + port;
        }
    }

    private KeyStore loadKeyStore(String trustStoreFileName, String trustStorePass) throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException {
        KeyStore keyStore = getInstance("JKS");
        try (InputStream keyStoreIs = new FileInputStream(trustStoreFileName)) {
            keyStore.load(keyStoreIs, trustStorePass.toCharArray());
            return keyStore;
        }
    }

    private boolean getSecureProp(Properties properties) throws NoPropertyException {
        String property = properties.getProperty(PropName.SECURE_CONNECTION.name);
        if (property != null) {
            if (property.toLowerCase().equals("true")) {
                return true;
            }
            if (property.toLowerCase().equals("false")) {
                return false;
            }
        }
        throw new NoPropertyException("no property for name '" + PropName.SECURE_CONNECTION.name + "'");
    }

    private String getStringProp(Properties properties, PropName propName) throws NoPropertyException {
        String property = properties.getProperty(propName.name);
        if (property == null) {
            throw new NoPropertyException("no property for name '" + propName.name + "'");
        }
        return property;
    }

    private HttpRequest createHttpRequest(String method) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        HttpRequest httpsRequest = null;
        String url = baseUrl + method;
        if (secureConnection) {
            httpsRequest = new HttpRequest(HttpRequest.Method.POST,
                    url, keyStore, keyStorePass, trustStore);
        } else {
            httpsRequest = new HttpRequest(HttpRequest.Method.POST, url);
        }
        return httpsRequest;
    }

    public PayCenterResponse makePay(KindPay kindPay, long amount, String comment) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        HttpRequest httpRequest = createHttpRequest(METHOD_MAKE_PAY);
        httpRequest.addParam("project_id", projectId);
        httpRequest.addParam("project_agent_id", projectAgentId);
        httpRequest.addParam("currency", currency);
        httpRequest.addParam("amount", amount + "");
        httpRequest.addParam("kind_pay", kindPay.name());
        httpRequest.addParam("comment", comment);
        String responseString = httpRequest.makeRequest();
        return GSON.fromJson(responseString, PayCenterResponse.class);
    }

    public PayCenterResponse getPaymentDetails(int payId) throws UnrecoverableKeyException, NoSuchAlgorithmException,
            KeyStoreException, KeyManagementException, IOException {
        HttpRequest httpsRequest = createHttpRequest(METHOD_GET_PAYMENT_DETAILS);
        httpsRequest.addParam("pay_id", payId + "");
        String responseString = httpsRequest.makeRequest();
        return GSON.fromJson(responseString, PayCenterResponse.class);
    }
}
