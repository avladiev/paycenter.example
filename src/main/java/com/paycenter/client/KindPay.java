package com.paycenter.client;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public enum KindPay {
    QIWI, TCS;

    public static KindPay getByName(String src) {
        String name = src.toUpperCase();
        for (KindPay kindPay : values()) {
            if (kindPay.name().equals(name)) {
                return kindPay;
            }
        }
        return null;
    }

}
