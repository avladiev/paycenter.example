package com.paycenter.client;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class PayCenterResponse {
    private String status;
    private String message;
    private PaymentDetails data;


    public String getResponseStatus() {
        return status;
    }

    public String getErrorMessage() {
        return message;
    }

    public PaymentDetails getPaymentDetails() {
        return data;
    }


    @Override
    public String toString() {
        return "PayCenterResponse{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
