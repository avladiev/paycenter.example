package com.paycenter.client;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpRequest {

    private static final String DEFAULT_CHARSET = "utf8";

    private final Map<String, String> myParams = new HashMap<String, String>();
    private String myUrl;
    private URLConnection myConnection;
    private Method myMethod;
    private static SSLSocketFactory sslSocketFactory;

    public HttpRequest(String url) {
        myUrl = url;
        myMethod = Method.POST;
    }

    public HttpRequest(Method method, String url) {
        this.myMethod = method;
        myUrl = url;
    }

    public HttpRequest(Method method, String url, KeyStore keyStore, String keyStorePass, KeyStore trustStore) throws NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, KeyManagementException {
        this.myMethod = method;
        myUrl = url;

        TrustManagerFactory tmf =
                TrustManagerFactory.getInstance("SunX509");
        tmf.init(trustStore);

        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(keyStore, keyStorePass.toCharArray());

        SSLContext context = SSLContext.getInstance("TLS");
        TrustManager[] trustManagers = tmf.getTrustManagers();
        context.init(kmf.getKeyManagers(), trustManagers, null);
        sslSocketFactory = context.getSocketFactory();
    }

    public void addParam(String key, String value) {
        myParams.put(key, value);
    }

    public String makeRequest() throws IOException {
        return makeRequest(true, null, null);
    }

    public void makeSimpleRequest() throws IOException {
        makeRequest(false, null, null);
    }

    public String makeRequest(boolean readData, String cookie) throws IOException {
        return makeRequest(readData, cookie, null);
    }

    public String makeRequest(boolean readData, String cookie, Map<String, String> headers) throws IOException {
        URL url = new URL(myUrl);
        myConnection = url.openConnection();
        myConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.202 Safari/535.1");
        myConnection.setConnectTimeout(30000);
        myConnection.setReadTimeout(30000);

        myConnection.setUseCaches(false);
        myConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

        if (cookie != null) {
            myConnection.setRequestProperty("Cookie", cookie);
        }

        if (headers != null) {
            for (String key : headers.keySet()) {
                myConnection.setRequestProperty(key, headers.get(key));
            }
        }


        if (myConnection instanceof HttpURLConnection) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) myConnection;
            httpURLConnection.setRequestMethod(myMethod.value);
            httpURLConnection.setInstanceFollowRedirects(false);

        }
        if (myConnection instanceof HttpsURLConnection) {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) myConnection;
            httpsURLConnection.setSSLSocketFactory(sslSocketFactory);
            httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {   //todo может так не стоит делать, но следить чтобы у сертификата был правельный хост мне надоело
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return s.equals(sslSession.getPeerHost());
                }
            });
        }

        if (myMethod == Method.POST) {
            String request = getRequestString();
            myConnection.setRequestProperty("Content-Length", Integer.toString(request.length()));
            myConnection.setDoOutput(true);

            PrintWriter writer = new PrintWriter(myConnection.getOutputStream());

            try {
                writer.write(request);
                writer.flush();
            } finally {
                writer.close();
            }
        }
        if (readData) {
            return getResponse(myConnection.getInputStream());
        } else {
            myConnection.getInputStream().close();
            return null;
        }

    }

    public String getRequestString() {
        //request payload
        if (myParams.size() == 1 && myParams.get("") != null) {
            return myParams.get("");
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : myParams.entrySet()) {
            if (sb.length() != 0) {
                sb.append("&");
            }
            try {
                sb.append(entry.getKey()).append("=").append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
        }
        return sb.toString();
    }

    public URLConnection getConnection() {
        return myConnection;
    }


    public int getResponseCode() throws IOException {
        return ((HttpURLConnection) myConnection).getResponseCode();
    }


    private String getResponse(InputStream s) throws IOException {
        String charset = getCharsetResponse();
        char[] c = new char[1024];
        StringBuilder out = new StringBuilder();
        Reader in = null;
        try {
            in = new InputStreamReader(s, charset);
            int nread;
            while ((nread = in.read(c)) != -1) {
                out.append(c, 0, nread);
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }

        return out.toString();
    }

    String getCharsetResponse() {
        List<String> propList = myConnection.getHeaderFields().get("Content-Type");
        if (propList != null && propList.size() > 0) {
            String prop = propList.get(0);
            for (String item : prop.split(";")) {
                if (item.contains("charset")) {
                    return item.split("=")[1].trim();
                }
            }
        }
        return DEFAULT_CHARSET;
    }

    public String getSession() throws IOException {
        makeSimpleRequest();
        return getCookie();
    }

    public String getCookie() {
        URLConnection connection = getConnection();
        List<String> cookies = connection.getHeaderFields().get("Set-Cookie");
        String result = "";
        for (String cookie : cookies) {
            if (!result.isEmpty()) {
                result += "; ";
            }
            result += cookie.substring(0, cookie.indexOf(";"));
        }
        return result;
    }

    public Map<String, String> getParams() {
        return myParams;
    }

    public enum Method {
        POST("POST"), GET("GET");
        String value;

        private Method(String value) {
            this.value = value;
        }
    }
}
