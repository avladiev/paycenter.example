package com.paycenter.client;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class PaymentDetails {
    private int payId;
    private int projectId;
    private int pcAgentId;
    private long timePay;
    private String status;
    private String commentPcAgent;

    public int getPayId() {
        return payId;
    }

    public int getProjectId() {
        return projectId;
    }

    public int getPcAgentId() {
        return pcAgentId;
    }

    public long getTimePay() {
        return timePay;
    }

    public String getStatus() {
        return status;
    }

    public String getCommentPcAgent() {
        return commentPcAgent;
    }

    @Override
    public String toString() {
        return "PaymentDetails{" +
                "payId=" + payId +
                ", projectId=" + projectId +
                ", pcAgentId=" + pcAgentId +
                ", timePay=" + timePay +
                ", status='" + status + '\'' +
                ", commentPcAgent='" + commentPcAgent + '\'' +
                '}';
    }
}
