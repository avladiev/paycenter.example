package com.paycenter.client;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class NoPropertyException extends Exception {
    public NoPropertyException() {
        super();
    }

    public NoPropertyException(String message) {
        super(message);
    }
}
